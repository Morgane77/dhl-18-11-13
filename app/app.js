'use strict';

var isformShown = true

function jsLoaded() {
    var jsloaded = document.querySelector('#is-js-loading');
    jsloaded.style.display = 'none';
    console.log('c\'est la balise que tu viens d\'enelever', jsloaded);
    initFormEditNote();
}
jsLoaded();

function initFormEditNote() {
    //console.log(arguments);
    var a = document.forms['form-edit_note'];
    //a.onsubmit = submitnote;
    a.addEventListener('submit', submitnote);
    a.addEventListener('reset', resetnote);
    document.forms['form-edit_note'].querySelectorAll('input:not(.btn),textarea,select').forEach(element => {
        element.addEventListener('focus', function(evt) {
            console.log();
            evt.target.classList.remove('invalid-input');
        });
    });
    document.querySelector('#img-cross img').addEventListener('click', toggleeditForm);
    isformShown = false;
    document.querySelector('.form-group').style.display = 'none';
    mymap = L.map('mapid').setView([51.505, -0.09], 13);
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 18,
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, '
        id: 'mapbox.streets'
    }).addTo(mymap);
}

function submitnote(evt) {
    if (undefined === evt) return;
    //on prend la main sur l'évènement
    evt.preventDefault();
    console.log(evt);
    var objNewNote = checkValidityOfForm();
    if (objNewNote == null) return;
    var img = objNewNote.userimg;
    delete(objNewNote.userimg);
    var name = objNewNote.username;
    delete(objNewNote.username);
    notes_restcrud.create(objNewNote, '/notes', function(response) {
        var noteComplete = JSON.parse(response);
        noteComplete.userimg = img;
        noteComplete.username = name;
        addNoteToList(noteComplete);
        document.forms["form-edit_note"].reset();
    });

}

function resetnote(evt) {
    evt.preventDefault();
    //document.forms['form-edit_note'].reset();
    var nodes = document.forms['form-edit_note'].querySelectorAll('input:not(.btn),textarea').forEach(function(element) { element.value = "" });
    document.forms['form-edit_note'].querySelectorAll('input:not(.btn),textarea,select').forEach(elem => { elem.classList.remove('invalid-input') });
    document.forms['form-edit_note']['input-user'].selectedIndex = -1;
}


//Vefirie si les champs sont remplis
function checkValidityOfForm() {

    var valid = true;

    var titre = document.forms["form-edit_note"]['input-titre'];
    if (titre.value.length <= 2) {
        valid = false;
        titre.classList.add('invalid-input');

    } else {
        titre.classList.remove('invalid-input');

    };

    var date = document.forms["form-edit_note"]['input-date'];
    if (undefined == date.value || (String(date.value).length != 10 || Date.parse(date.value) == undefined)) {
        valid = false;
        date.classList.add('invalid-input');
    } else {
        date.classList.remove('invalid-input');
    }

    var time = document.forms["form-edit_note"]['input-time'];
    const regex = /^([01]\d|2[0-3]):[0-5]\d$/;
    if (regex.exec(time.value) == null) {
        valid = false;
        time.classList.add('invalid-input');
    } else {
        time.classList.remove('invalid-input');
    }
    var description = document.forms["form-edit_note"]['input-description'];
    if (description.value == "") {
        valid = false;
        description.classList.add('invalid-input');
    } else {
        description.classList.remove('invalid-input');
    }

    var user = document.forms["form-edit_note"]['input-user'];
    var userName = '';
    var userid = '';
    if (-1 == user.selectedIndex || user.value == '') {
        valid = false;
        user.classList.add('invalid-input');
    } else {
        user.classList.remove('invalid-input');
        var infosUser = user.options[user.selectedIndex].innerHTML.split(':');
        userid = infosUser[0];
        userName = infosUser[1];
    }


    console.log(titre.value, date.value, time.value, description.value, user.value);

    return (valid) ? {
        titre: titre.value,
        date: date.value,
        time: time.value,
        description: description.value,
        userimg: user.value,
        user: userid,
        username: userName
    } : null;

}
//checkValidityOfForm();

function addNoteToList(objNoteToAdd) {
    //on vérifie 
    if (undefined === objNoteToAdd &&
        ((objNoteToAdd = checkValidityOfForm()) == undefined)) return false;
    console.log(objNoteToAdd);


    var maDivNote = document.createElement('div');
    maDivNote.classList.add('note');
    maDivNote.id = 'note-' + objNoteToAdd.id;
    maDivNote.innerHTML = '<div class="img-close"><img src="img/close.png" /></div><div class="note-photo-user"><img src="img/' + objNoteToAdd.userimg + '"><br>' + objNoteToAdd.user + ':' + objNoteToAdd.username + '</div><div class="note-right"><div class="note-titre">' +
        objNoteToAdd.titre + '</div><div     class="note-datetime">' +
        objNoteToAdd.date + ' à ' +
        objNoteToAdd.time + '</div></div><div     class="note-description">' +
        objNoteToAdd.description + '</div>';
    maDivNote.querySelector('.img-close img').addEventListener('dblclick', deletenote);
    document.querySelector('#notes-container').appendChild(maDivNote);
    return true;

}

function deletenote(evt) {
    console.log(evt.target);
    var isOkToDelete = confirm('Souhaitez vous vraiment supprimer cette note');
    if (isOkToDelete) {
        var idnote = evt.target.parentElement.parentElement.id.split('-')[1];
        notes_restcrud.delete('/notes/' + idnote, function() {
            alert('deletion OK');
            evt.target.parentElement.parentElement.remove();
        });
    }
}

function toggleeditForm() {
    isformShown = !isformShown;
    document.querySelector('.form-group').style.display = (isformShown) ? 'block' : 'none';
}

var notes_restcrud = new ResTCRUD('http://localhost:7500');
notes_restcrud.read('/notes', function(responseText) {
    var myParseOfJson = JSON.parse(responseText);
    //console.log(myParseOfJson);
    myParseOfJson.map(elem => {
        notes_restcrud.read('/users/' + elem.user, function(resp) {
            var userObJResponse = JSON.parse(resp);
            //elem.iduser = elem.user;
            elem.userimg = userObJResponse.img;
            elem.username = userObJResponse.name;
            console.log(elem);
            addNoteToList(elem);
        });
    });
});